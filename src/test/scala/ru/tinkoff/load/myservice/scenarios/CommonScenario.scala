package ru.tinkoff.load.myservice.scenarios

import io.gatling.core.Predef._
import io.gatling.core.structure.ScenarioBuilder
import ru.tinkoff.load.myservice.cases._
import ru.tinkoff.load.myservice.feeders.Feeders._

object CommonScenario {
  def apply(): ScenarioBuilder = new CommonScenario().scn
}

class CommonScenario {

  val tableName = "Accounts"

  val scn: ScenarioBuilder = scenario("Common Scenario")
    .feed(account)
    .feed(amount)
    .exec(Actions.selectTable(tableName))
    .randomSwitch(
      50.0 -> exec(Actions.insertTableDebit(tableName)),
      50.0 -> exec(Actions.insertTableCredit(tableName))
    )
    .exec(Actions.updateTable(tableName))
    .exec(Actions.updateTableAll(tableName))
    //.exec(Actions.deleteTable(tableName))
}